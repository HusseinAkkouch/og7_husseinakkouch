
public class Addon {

	private String bezeichner;
	private int idNummer;
	private double verkaufspreis;
	private int maxbestand;
	
	public String getBezeichner() {
		return bezeichner;
	}
	
	public void setBezeichner(String bezeichner) {
		this.bezeichner = bezeichner;
	}
	
	public int getIdNummer() {
		return idNummer;
	}
	
	public double getVerkaufspreis() {
		return verkaufspreis;
	}
	
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	
	public int getMaxbestand() {
		return maxbestand;
	}
	
	public void setMaxbestand(int maxbestand) {
		this.maxbestand = maxbestand;
	}

	public Addon(String bezeichner, int idNummer, double verkaufspreis, int maxbestand) {
		super();
		this.bezeichner = bezeichner;
		this.idNummer = idNummer;
		this.verkaufspreis = verkaufspreis;
		this.maxbestand = maxbestand;
	}


}
