
public class HauptFenster {
	private int[] datum;
	private String fach;
	private String beschriebung;
	private int dauer;

	public int[] getDatum() {
		return datum;
	}

	
	public HauptFenster(int[] datum, String fach, String beschriebung, int dauer) {
		super();
		this.datum = datum;
		this.fach = fach;
		this.beschriebung = beschriebung;
		this.dauer = dauer;
	}

	
	public void setDatum(int[] datum) {
		this.datum = datum;
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschriebung() {
		return beschriebung;
	}

	public void setBeschriebung(String beschriebung) {
		this.beschriebung = beschriebung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

}
