
public class Schiedsrichter extends Person {

	// attribute

	public static int anzahlspiele;
	public static String namespiele;

	// Methoden

	public Schiedsrichter(String name, String telNum, boolean jahresbetrag, int anzahlspiele, String namespiele) {
		super(name, telNum, jahresbetrag);
		this.anzahlspiele=anzahlspiele;
		this.namespiele=namespiele;
	}

	public int getAnzahlspiele() {

		return anzahlspiele;
	}

	public void setAnzahlspiele(int anzahlspiele) {
		this.anzahlspiele = anzahlspiele;
	}

	public String getNamespiele() {
		return namespiele;
	}

	public void setNamespiele(String namespiele) {
		this.namespiele = namespiele;
	}
	
	public static void gibSchiriAus(Schiedsrichter testSchiri) {
		String ausgabe=name+" ," +telNum+" ,"+String.valueOf(jahresbetrag)+" ,"+ Integer.toString(anzahlspiele)+" ,"+namespiele;
		
		
		System.out.println(ausgabe);
			
		
	}

}
