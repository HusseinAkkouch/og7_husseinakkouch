
/**
  *
  * Übungsklasse zu Feldern
  *
  * @version 1.0 vom 05.05.2011
  * @author Tenbusch
  */
import java.util.Scanner;

import com.sun.xml.internal.fastinfoset.util.ValueArray;

public class Felder {

	// unsere Zahlenliste zum Ausprobieren
	private int[] zahlenliste = { 5, 8, 4, 3, 9, 1, 2, 7, 6, 0 };
	Scanner scan = new Scanner(System.in);

	// Konstruktor
	public Felder() {
	}

	// Methode die Sie implementieren sollen
	// ersetzen Sie den Befehl return 0 durch return ihre_Variable

	// die Methode soll die größte Zahl der Liste zurückgeben
	public int maxElement() {
		int max = 0;
		for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i] > max) {
				max = zahlenliste[i];
			}

		}
		return max;
	}

	// die Methode soll die kleinste Zahl der Liste zurückgeben
	public int minElement() {
		int min = 0;
		for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i] < min) {
				min = zahlenliste[i];
			}

		}
		return min;
	}

	// die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
	public double durchschnitt() {
		int all = 0;
		double dur = zahlenliste.length;
		for (int i = 0; i < zahlenliste.length; i++) {

			all += zahlenliste[i];
		}
		dur = all / dur;
		return dur;
	}

	// die Methode soll die Anzahl der Elemente zurückgeben
	// der Befehl zahlenliste.length; könnte hierbei hilfreich sein
	public int anzahlElemente() {
		int dur = zahlenliste.length;
		return dur;
	}

	// die Methode soll die Liste ausgeben
	public String toString() {
		String liste = "";
		int eingabe = scan.nextInt();
		
		for (int i = 1; i < eingabe; i++) {
			zahl1[i] = i;
			liste += zahl1[i];

		}

		return liste;
	}

	// die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
	// dem Feld vorhanden ist
	public boolean istElement(int zahl) {
		
		
		return false;
	}

	// die Methode soll das erste Vorkommen der
	// als Parameter übergebenen Zahl liefern oder -1 bei nicht vorhanden
	public int getErstePosition(int zahl) {
		return 0;
	}

	// die Methode soll die Liste aufsteigend sortieren
	// googlen sie mal nach Array.sort() ;)
	public void sortiere() {

	}

	public static void main(String[] args) {
		Felder testenMeinerLösung = new Felder();
		System.out.println(testenMeinerLösung.maxElement());
		System.out.println(testenMeinerLösung.minElement());
		System.out.println(testenMeinerLösung.durchschnitt());
		System.out.println(testenMeinerLösung.anzahlElemente());
		System.out.println(testenMeinerLösung.toString());
		System.out.println(testenMeinerLösung.istElement(9));
		System.out.println(testenMeinerLösung.getErstePosition(5));
		testenMeinerLösung.sortiere();
		System.out.println(testenMeinerLösung.toString());
	}
}
