package klausur;

import java.util.Random;

public class Klausur2 {

	public final int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		Klausur2 k2 = new Klausur2();
		int n = 10; // Anzahl der Stellen

		// Erstellt und sortiert Array mit n Stellen
		long[] zahlen = k2.getSortedLongArray(n);

		// ----------Hier in die Main k�nnen Sie Ihre Analyse f�r das Protokoll
		// schreiben--------------
		for(long l:zahlen)
			System.out.println(k2.schlaubiSchlumpfSuche(zahlen, l));
		
	}

	public int lineareSuche(long[] zahlen, long gesuchteZahl) {
		for (int i = 0; i < zahlen.length; i++)
			if (zahlen[i] == gesuchteZahl)
				return i;
		return NICHT_GEFUNDEN;
	}

	public int schlaubiSchlumpfSuche(long[] zahlen, long gesuchteZahl) {
		int links = 0;
		int rechts = zahlen.length / 4;
		for (int viertel = 1; viertel < 4; viertel++) {
			if (zahlen[rechts] < gesuchteZahl) {
				links = rechts;
				rechts = (viertel + 1) * zahlen.length / 4 - 1;
			}
		}
		while (rechts >= links){
			if (gesuchteZahl == zahlen[rechts])
				return rechts;
			else
				rechts--;
		}
		return NICHT_GEFUNDEN;
	}

	public int binaereSuche(long[] zahlen, long gesuchteZahl) {
		int l = 0, r = zahlen.length - 1;
		int m;

		while (l <= r) {
			// Bereich halbieren
			m = l + ((r - l) / 2);

			if (zahlen[m] == gesuchteZahl) // Element gefunden?
				return m;

			if (zahlen[m] > gesuchteZahl)
				r = m - 1; // im linken Abschnitt weitersuchen
			else
				l = m + 1; // im rechten Abschnitt weitersuchen
		}
		return NICHT_GEFUNDEN;
	}

	// -----------------------Finger weg von diesem Teil des Codes

	/**
	 * Methode f�llt das Attribut zahlen mit einem Array der L�nge des
	 * Paramaters z.B. getIntArray(1000) gibt ein 1000-stelliges Array zur�ck
	 * 
	 * @param stellen
	 *            - Anzahl der zuf�lligen Stellen
	 * @return das Array
	 */
	public long[] getSortedLongArray(int stellen) {
		Random rand = new Random(666L);
		long[] zahlen = new long[stellen];
		for (int i = 0; i < stellen; i++)
			zahlen[i] = rand.nextInt(20000000);
		this.radixSort(zahlen);
		return zahlen;
	}

	/**
	 * Super effizienter Algorithmus zur Sortierung der Zahlen Laufzeit von
	 * //NoHintAvailable
	 */
	public void radixSort(long[] zahlen) {
		long[] temp = new long[20000000];
		for (long z : zahlen)
			temp[(int) z]++;
		int stelle = 0;
		for (int i = 0; i < temp.length; i++)
			if (temp[i] != 0)
				while (temp[i]-- != 0)
					zahlen[stelle++] = i;
	}

}
