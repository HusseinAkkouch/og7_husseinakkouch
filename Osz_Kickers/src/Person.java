
abstract public class Person {

	public static String name;
	public static String telNum;
	public static boolean jahresbetrag;

	public Person(String name, String telNum, boolean jahresbetrag) {
		this.name = name;
		this.telNum = telNum;
		this.jahresbetrag = jahresbetrag;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getTelNum() {

		return telNum;
	}

	public void setTelNum(String telNum) {

		this.telNum = telNum;
	}

	public boolean isJahresbetrag() {

		return jahresbetrag;
	}

	public void setJahresbetrag(boolean jahresbetrag) {

		this.jahresbetrag = jahresbetrag;
	}

}
