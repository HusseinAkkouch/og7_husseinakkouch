
public class Einheit {

	private String name;
	private int gold;
	private String zeichenkette;

	
	
	
	public Einheit(String name, int gold, String zeichenkette) {
		super();
		this.name = name;
		this.gold = gold;
		this.zeichenkette = zeichenkette;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public String getZeichenkette() {
		return zeichenkette;
	}

	public void setZeichenkette(String zeichenkette) {
		
		this.zeichenkette = zeichenkette;
	}

	@Override
	public String toString() {
		return "Einheit [name=" + name + ", gold=" + gold + ", zeichenkette=" + zeichenkette + "]";
	}


	

}
