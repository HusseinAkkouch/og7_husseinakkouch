import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JButton;

public class ClashGui extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClashGui frame = new ClashGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClashGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 4, 0, 0));
		
		JLabel lblClashOfEgal = new JLabel("Clash of Egal");
		contentPane.add(lblClashOfEgal);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		
		JLabel lblBild = new JLabel("");
		panel.add(lblBild);
		
		JLabel lblName = new JLabel("babar");
		panel.add(lblName);
		
		JButton btnKaufen = new JButton("Kaufen");
		panel.add(btnKaufen);
		
		JLabel lblKosten = new JLabel("4");
		panel.add(lblKosten);
	}

}
