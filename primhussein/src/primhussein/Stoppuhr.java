package primhussein;

public class Stoppuhr {

	private long start ;
	private long stopp ;
	
	
	public void Start() {
		this.start = System.currentTimeMillis();
	}
	
	public void Stopp() {
		this.stopp = System.currentTimeMillis();
	}
	
	public long getMs() {
	long ms = stopp - start;
		return ms;
	}
	
	public Stoppuhr() {
		super();
	}
	
	public void reset() {
	this.start = 0;
	this.stopp = 0;
	}
	
	
}
