package omnom;

public class Haustier {
	public int hunger = 100;
	public int muede = 100;
	public int zufrieden = 100;
	public int gesund = 100;

	public Haustier() {
	super();
	}

	public void fuettern(int anzahl) {
	this.setHunger(this.getHunger()+anzahl);

	}

	public void schlafen(int dauer ) {
		this.setMuede(this.getMuede()+ dauer);

	}

	public void spielen(int zufrieden) {
		zufrieden += 25;
	}

	public void heilen(int gesund) {

		gesund += 1;
	}

	public int getHunger() {
		return zufrieden;

	}

	public int getMuede() {
		return muede;

	}

	public int getZufrieden() {
		return zufrieden;
	}

	public int getGesund() {

		return gesund;
	}

	public String getName() {
		return "cat";
	}

	public void setGesund(int gesundheit) {

		this.gesund = gesundheit;
		if (gesundheit > 100) {
			gesundheit = 100;
		}
	}

	public void setZufrieden(int zufrieden) {
		this.zufrieden = zufrieden;
		if (zufrieden > 100) {
			zufrieden = 100;
		}
	}

	public void setMuede(int schlaf) {

		this.muede = schlaf;
		if (schlaf > 100) {
			schlaf = 100;
		}
	}

	public void setHunger(int hunger) {
		this.hunger = hunger;
		if (hunger > 100) {
			hunger = 100;
		}

	}
}
