
public class Manschaftsleiter extends Spieler {

	// attribute

	public String manschaftsname;
	public double enagement;

	// Methoden

	public String getManschaftsname() {
		return manschaftsname;
	}

	public void setManschaftsname(String manschaftsname) {
		this.manschaftsname = manschaftsname;
	}

	public double getEnagement() {
		return enagement;
	}

	public void setEnagement(double enagement) {
		this.enagement = enagement;
	}

}
